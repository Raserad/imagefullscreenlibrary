package com.raserad.apps.imagefullscreenslider.views;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import com.raserad.apps.imagefullscreenslider.activities.ImageSliderFullscreen;
import com.raserad.apps.imagefullscreenslider.adapter.ImageListAdapter;
import com.raserad.apps.imagefullscreenslider.listeners.ItemClickListener;

import java.util.ArrayList;


public class ImageSliderList extends RecyclerView {

    ImageListAdapter adapter;

    private ArrayList<String> list;

    public void setList(ArrayList<String> list) {
        this.list = list;
        adapter.setList(list);
        adapter.notifyDataSetChanged();
    }

    public ImageSliderList(Context context) {
        super(context);
    }

    public ImageSliderList(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ImageSliderList(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        adapter = new ImageListAdapter(new ItemClickListener() {
            @Override
            public void onClick(int position) {
                showFullscreen(position);
            }
        });
        setAdapter(adapter);
    }

    private void showFullscreen(int position) {
        Intent intent = new Intent(getContext(), ImageSliderFullscreen.class);
        intent.putStringArrayListExtra("list", list);
        intent.putExtra("position", position);
        getContext().startActivity(intent);
    }
}
