package com.raserad.apps.imagefullscreenslider.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.raserad.apps.imagefullscreenslider.R;
import com.raserad.apps.imagefullscreenslider.listeners.ItemClickListener;

import java.util.List;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageHolder> {

    private final ItemClickListener itemClickListener;
    private List<String> list;

    public void setList(List<String> list) {
        this.list = list;
    }

    public ImageListAdapter(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_photo_item, parent, false);
        return new ImageHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageHolder holder, int position) {
        holder.show(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    class ImageHolder extends RecyclerView.ViewHolder {

        ImageView imageView;

        ImageHolder(View itemView) {
            super(itemView);
            initViews();
        }

        private void initViews() {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });

            imageView = itemView.findViewById(R.id.image);
        }

        void show(String url) {
            Glide.with(itemView)
                    .load(url)
                    .into(imageView);
        }
    }
}
