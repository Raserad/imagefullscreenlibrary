package com.raserad.apps.imagefullscreenslider.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.raserad.apps.imagefullscreenslider.R;

import java.util.List;

public class ImageSliderFullscreen extends AppCompatActivity {

    private List<String> list;
    private int position;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.component_image_slider);

        list = getIntent().getStringArrayListExtra("list");
        position = getIntent().getIntExtra("position", 0);
        initView();
    }

    private void initView() {
        ViewPager pager = findViewById(R.id.image_slider_pager);
        SlideAdapter adapter = new SlideAdapter(getSupportFragmentManager());
        if(list.size() > 0) {
            for (String url: list) {
                FullscreenSlide slide = new FullscreenSlide();
                slide.setUrl(url);
                adapter.addFragment(slide);
            }
        }
        if(list.size() > 1) {
            TabLayout tabLayout = findViewById(R.id.image_slider_indicator);
            tabLayout.setupWithViewPager(pager);
        }
        pager.setAdapter(adapter);
        pager.setCurrentItem(position);
    }
}
