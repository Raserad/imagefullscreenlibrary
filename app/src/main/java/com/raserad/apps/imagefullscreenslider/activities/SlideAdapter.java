package com.raserad.apps.imagefullscreenslider.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SlideAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> fragmentList = new ArrayList<>();
    private int position = 0;

    SlideAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        this.position = position;
        return fragmentList.size() > 0 ? fragmentList.get(position) : null;
    }

    public void clear(){
        fragmentList.clear();
    }

    void addFragment(Fragment fragment){
        fragmentList.add(fragment);
    }

    public void removeFragment(Fragment fragment){
        fragmentList.remove(fragment);
        notifyDataSetChanged();
    }

    public List<Fragment> getList() {
        return fragmentList;
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "";
    }

    public int getPosition() {
        return position;
    }
}