package com.raserad.apps.imagefullscreenslider.listeners;

public interface ItemClickListener {

    void onClick(int position);
}
